﻿using UnityEngine;
using System.Collections;

public class BaseJumpAttack : MonoBehaviour {

    protected BaseMain baseMain;

    public GameObject JumpHit;
    public AudioClip AttackSound;
    protected float raycastRange;
    protected LayerMask isGroundMask;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
        raycastRange = baseMain.raycastRange;
        isGroundMask = baseMain.isGroundMask;
    }
    void OnEnable()
    {
        JumpHit.SetActive(true);
        baseMain.animator.SetBool("JumpAttack", false);
        if (AttackSound != null)
        {
            baseMain.audioSource.PlayOneShot(AttackSound);
        }
    }
    void OnDisable()
    {
        JumpHit.SetActive(false);
    }

    void FixedUpdate()
    {
        if (IsGrounded())
        {
            //baseMain.fallSpeed = -1;
            baseMain.animator.SetBool("JumpAir", false);
        }
        if (!IsGrounded())
        {
            //baseMain.fallSpeed += Physics.gravity.y * Time.deltaTime;
            baseMain.movementV.y = baseMain.movementV.y + (Physics.gravity.y * Time.deltaTime);
            baseMain.movementV.x = baseMain.xairspeed * 0.7f;
            baseMain.movementV.z = baseMain.zairspeed * 0.7f;
            GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);
        }
    }
    bool IsGrounded() //verify if character is touching the ground
    {
        return Physics.Raycast(transform.position, -Vector3.up, raycastRange, isGroundMask);
    }
}
