﻿using UnityEngine;
using System.Collections;

public class BaseKnockBack : MonoBehaviour {

    protected BaseMain baseMain;

    public int didLaunch = 0;
    protected float raycastRange;
    protected LayerMask isGroundMask;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
        raycastRange = baseMain.raycastRange;
        isGroundMask = baseMain.isGroundMask;
    }

    void OnEnable()
    {
        baseMain.movementV = transform.Find("CharacterHitBox").GetComponent<CharacterHitReceive>().KnockBackPass;
        //baseMain.fallSpeed = transform.Find("CharacterHitBox").GetComponent<CharacterHitReceive>().KnockBackPass.y;
        GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);
        Debug.Log("started Knockback");
    }
    void FixedUpdate()
    {
        if (IsGrounded())// && didLaunch >= 2)
        {
            //baseMain.fallSpeed = -1;
            baseMain.animator.SetBool("KnockBack", false);
            //didLaunch = 0;
        }
        else// if (!IsGrounded())
        {
            /*baseMain.fallSpeed += Physics.gravity.y * Time.deltaTime;
            baseMain.movementV.y = baseMain.fallSpeed;*/
            baseMain.movementV.y = baseMain.movementV.y + (Physics.gravity.y * Time.deltaTime);
            GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);
        }
        //didLaunch++;
    }
    bool IsGrounded() //verify if character is touching the ground
    {
        Debug.DrawRay(transform.position, -Vector3.up, Color.cyan, raycastRange);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, raycastRange, isGroundMask))
        {
            Debug.Log("ground is: "+hit.collider.gameObject.name);
            return true;
        }
        else
        {
            return false;
        }
        
        //return Physics.Raycast(transform.position, -Vector3.up, 0.33f);
    }
}
