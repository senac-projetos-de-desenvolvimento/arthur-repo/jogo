﻿using UnityEngine;
using System.Collections;

public class BaseAttackComboEnder : MonoBehaviour {

	protected BaseMain baseMain;

	public GameObject Hit;
	public AudioClip AttackSound;

	private void Awake()
	{
		baseMain = GetComponent<BaseMain>();
	}

	void OnEnable ()
	{
		baseMain.animator.SetBool("Attack", false);
		Hit.SetActive(true);
		if (AttackSound != null)
		{
			baseMain.audioSource.PlayOneShot(AttackSound);
		}
	}

	void OnDisable() 
	{
		Hit.SetActive(false);
	}
}
