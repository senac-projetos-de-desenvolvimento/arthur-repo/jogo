﻿using UnityEngine;
using System.Collections;

public class BaseJump : MonoBehaviour {

	protected BaseMain baseMain;

	protected float raycastRange;
	protected LayerMask isGroundMask;

	private void Awake()
	{
		baseMain = GetComponent<BaseMain>();
		raycastRange = baseMain.raycastRange;
		isGroundMask = baseMain.isGroundMask;
	}

	void Start (){
		baseMain.movementV.x = baseMain.xairspeed;
		baseMain.movementV.z = baseMain.zairspeed;
	}

	// Update is called once per frame
	void Update () {
        if (baseMain.attack)
        {
            baseMain.animator.SetBool("JumpAttack", true);
        }
	}
	void FixedUpdate (){
		if (IsGrounded()) {
			//baseMain.fallSpeed = -1;
			baseMain.animator.SetBool("JumpAir", false);
		}
		if (!IsGrounded()) {
			/*baseMain.fallSpeed += Physics.gravity.y * Time.deltaTime;
			baseMain.movementV.y = baseMain.fallSpeed;*/
			baseMain.movementV.y = baseMain.movementV.y + (Physics.gravity.y * Time.deltaTime);
			baseMain.movementV.x = baseMain.xairspeed * 0.7f;
			baseMain.movementV.z = baseMain.zairspeed * 0.7f;
			GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);


		}

		//rotate
		if (baseMain.rotatedRight && !baseMain.rotatedLeft)
		{
			//GetComponent<Transform>().localScale = new Vector3( 2,2,0.5f);
			//GetComponent<Transform>().localScale = new Vector3(2, GetComponent<Transform>().localScale.y, GetComponent<Transform>().localScale.z);
			transform.rotation = Quaternion.Euler(0, 0, 0);
			baseMain.shadow.transform.rotation = Quaternion.Euler(120, 0, 0);
			baseMain.animator.gameObject.transform.rotation = Quaternion.Euler(30, 0, 0);
			baseMain.animator.gameObject.GetComponent<SpriteRenderer>().flipX = false;
		}
		else if (baseMain.rotatedLeft && !baseMain.rotatedRight)
		{
			//GetComponent<Transform>().localScale = new Vector3(-2,2,0.5f);
			//GetComponent<Transform>().localScale = new Vector3(-2, GetComponent<Transform>().localScale.y, GetComponent<Transform>().localScale.z);
			transform.rotation = Quaternion.Euler(0, -180, 0);
			baseMain.shadow.transform.rotation = Quaternion.Euler(120, 0, 0);
			baseMain.animator.gameObject.transform.rotation = Quaternion.Euler(30, 0, 0);
			baseMain.animator.gameObject.GetComponent<SpriteRenderer>().flipX = true;
		}
	}
	bool IsGrounded () //verify if character is touching the ground
	{
		return Physics.Raycast (transform.position, - Vector3.up, raycastRange, isGroundMask);
	}
}
