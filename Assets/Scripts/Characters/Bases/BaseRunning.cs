﻿using UnityEngine;
using System.Collections;

public class BaseRunning : MonoBehaviour {

	protected BaseMain baseMain;

	private void Awake()
	{
		baseMain = GetComponent<BaseMain>();
	}

	// Update is called once per frame
	void Update () {

		if (baseMain.runningL && !baseMain.runningR) 
		{
			baseMain.movementV.x = baseMain.movement.runspeed *  -1;
			if(baseMain.rightRun)
			{
                baseMain.rotatedLeft = true;
                baseMain.rotatedRight = false;
                baseMain.runningL = false;
				baseMain.runningR = false;
				baseMain.animator.SetBool("Running", false);
				//rotate
				/*transform.rotation = Quaternion.Euler(0, 0, 0);
				baseMain.shadow.transform.rotation = Quaternion.Euler(120, 0, 0);
				baseMain.animator.gameObject.transform.rotation = Quaternion.Euler(30, 0, 0);
				baseMain.animator.gameObject.GetComponent<SpriteRenderer>().flipX = false;*/

			}
		}
		else if (!baseMain.runningL && baseMain.runningR) 
		{
			baseMain.movementV.x = baseMain.movement.runspeed *   1;
			if(baseMain.leftRun)
			{
                baseMain.rotatedLeft = false;
                baseMain.rotatedRight = true;
                baseMain.runningL = false;
				baseMain.runningR = false;
				baseMain.animator.SetBool("Running", false);
				//rotate
				/*transform.rotation = Quaternion.Euler(0, -180, 0);
				baseMain.shadow.transform.rotation = Quaternion.Euler(120, 0, 0);
				baseMain.animator.gameObject.transform.rotation = Quaternion.Euler(30, 0, 0);
				baseMain.animator.gameObject.GetComponent<SpriteRenderer>().flipX = true;*/
			}
		}
		// Z movement
		if (baseMain.up && !baseMain.down) {
			baseMain.movementV.z = baseMain.movement.zspeed *  1;
		} else if (baseMain.down && !baseMain.up) {
			baseMain.movementV.z = baseMain.movement.zspeed * -1;
		} else {
			baseMain.movementV.z = 0;
		}
        //attack
        if (baseMain.attack)
        {
            baseMain.animator.SetBool("Attack", true);
        }
	}
	void FixedUpdate (){
		GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);
	}
}
