﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BaseMain : MonoBehaviour {
	public string playerN; //Player's Number
    public int charaT; //Character's Team

	public Sprite Portrait;
	[System.Serializable]
	public class HealthC
	{
		public float HealthMax = 1000;
		public float HealthRemaining;
		public Image HealthBar;
		public Image BotHealthBar;
	}

	[System.Serializable]
	public class ManaC
	{
		public float ManaMax = 1000;
		public float ManaRemaining;
		public Image ManaBar;
		public float SpecialCost = 500;
	}

	[System.Serializable]
	public class MovementC //Character's movement stats
	{
		public float xspeed = 2;
		public float zspeed = 1;
		public float runspeed = 4;
		public float jumpPower = 6.5f;
	}

	//Special Status
	[System.Serializable]
	public class SpecialStatus
	{
		public bool armored = false;
	}

	[System.NonSerialized] public int hitconfirmed = 0;

	//Used to check if running
	[System.NonSerialized] public bool runningL = false;
	[System.NonSerialized] public bool runningR = false;

	//Current movement
	[System.NonSerialized] public Vector3 movementV;

	//Used to store and keep momentum from before jumping
	[System.NonSerialized] public float xairspeed = 0;
	[System.NonSerialized] public float zairspeed = 0;

	//rotation
	[System.NonSerialized] public bool rotatedRight = true;
	[System.NonSerialized] public bool rotatedLeft = false;

	[System.NonSerialized] public AudioSource audioSource;
	public Animator animator;
	public GameObject shadow;

	public HealthC Health = new HealthC();
	public ManaC Mana = new ManaC();
	public MovementC movement = new MovementC();
	public SpecialStatus specialStatus = new SpecialStatus();

	public float raycastRange = 0.22f;
	public LayerMask isGroundMask = 1;

	[System.NonSerialized] public GameObject[] childs;
	protected GameObject spriteObject;

	//Inputs
	[System.NonSerialized] public bool attack;
	[System.NonSerialized] public bool special;
	[System.NonSerialized] public bool jump;
	[System.NonSerialized] public bool defend;
	[System.NonSerialized] public bool right;
	[System.NonSerialized] public bool left;
	[System.NonSerialized] public bool up;
	[System.NonSerialized] public bool down;
	[System.NonSerialized] public bool rightRun; //Running (get down)
	[System.NonSerialized] public bool leftRun;  //Running (get down)

	// Use this for initialization
	void Start () {
		Health.HealthRemaining = Health.HealthMax;
		childs = new GameObject[transform.childCount];
        Debug.Log("CharaT "+charaT);
        Debug.Log("LayerMaskH "+LayerMask.NameToLayer("Team" + charaT + "Hit"));
        Debug.Log("LayerMaskHB " + LayerMask.NameToLayer("Team" + charaT + "CharactersHitboxs"));
		//Sets the team for all HitBoxes
        for (int i = 0; i < transform.childCount; i++)
        {
            childs[i] = transform.GetChild(i).gameObject;

			if (childs[i].tag == "Hit")
			{
				childs[i].layer = LayerMask.NameToLayer("Team" + charaT + "Hit");
			}
			if (childs[i].tag == "CharacterHitBox")
			{
				childs[i].layer = LayerMask.NameToLayer("Team" + charaT + "CharactersHitboxs");
			}


			//Debug.Log("Child "+childs[i].name);
		}
		if(playerN == "Bot")
        {
			//Sets the Character HealthBar
			gameObject.tag = "Bot";
			Health.BotHealthBar.gameObject.transform.parent.gameObject.SetActive(true);
			Health.HealthBar = Health.BotHealthBar;
		}
        else
        {
			//Activates The Player UI
			GameObject.Find("Canvas").transform.Find("P" + playerN + "UI").gameObject.SetActive(true);
			//Sets the Character Portrait
			GameObject.Find("Portrait" + playerN).GetComponent<Image>().sprite = Portrait;
			//Sets the Character HealthBar
			Health.HealthBar = GameObject.Find("Hp" + playerN).GetComponent<Image>();
			//Sets the Character ManaBar
			Mana.ManaBar = GameObject.Find("Mp" + playerN).GetComponent<Image>();
			Mana.ManaRemaining = 0;
		}

		//Sets the GameObject according to the character's player
		gameObject.name = gameObject.name.Replace('0', playerN[0]);
		//gameObject.name = gameObject.name.Replace('0', (char)(charaT + 48));
		//Sets the character's animator
		spriteObject = animator.gameObject;
		//Sets The Audio Source
		audioSource = gameObject.GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		if(playerN == "Bot")
        {

        }
        else
        {
			//Debug.Log("This is Player " + playerN);
			//Gets inputs
			special = Input.GetButton("P" + playerN + "Special");
			attack = Input.GetButtonDown("P" + playerN + "Attack");
			jump = Input.GetButtonDown("P" + playerN + "Jump");
			defend = Input.GetButtonDown("P" + playerN + "Defend");
			right = Input.GetButton("P" + playerN + "Right");
			left = Input.GetButton("P" + playerN + "Left");
			up = Input.GetButton("P" + playerN + "Up");
			down = Input.GetButton("P" + playerN + "Down");
			rightRun = Input.GetButtonDown("P" + playerN + "Right");
			leftRun = Input.GetButtonDown("P" + playerN + "Left");
		}
		

		Health.HealthBar.fillAmount = Health.HealthRemaining/ Health.HealthMax;
		if (playerN != "Bot")
        {
			Mana.ManaBar.fillAmount = Mana.ManaRemaining / Mana.ManaMax;
        }
		/*rotatedRight = right;
		rotatedLeft = left;*/
		if(rotatedLeft && right && !left)
        {
			rotatedLeft = false;
			rotatedRight = true;
        }else if(rotatedRight && left && !right)
        {
			rotatedRight = false;
			rotatedLeft = true;
        }
        else
        {

        }
		if(rotatedLeft || rotatedRight)
		{

		}
		if (Health.HealthRemaining < 1)
		{
			animator.SetBool("Dead", true);
			//Destroy(gameObject);
		}
    }
	void FixedUpdate (){
		if(Mana.ManaRemaining < 1000)
        {
			Mana.ManaRemaining++;
		}
		if(hitconfirmed > 0)
        {
			hitconfirmed--;
        }
	}
}
