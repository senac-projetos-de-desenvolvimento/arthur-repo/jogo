﻿using UnityEngine;
using System.Collections;

public class BaseDead : MonoBehaviour {

    protected BaseMain baseMain;

    protected SpriteRenderer spriteRenderer;

    protected float timeSinceBotDeath = 0;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
        spriteRenderer = baseMain.animator.gameObject.GetComponent<SpriteRenderer>();
    }

    void OnEnable ()
    {
        if(gameObject.tag == "Player")
        {
            gameObject.tag = "PlayerDead";
        }
	}

    void Update()
    {
        if (gameObject.tag == "Bot")
        {
            Debug.Log("This bot is dead "+ timeSinceBotDeath);
            if (timeSinceBotDeath >= 2)
            {
                if (spriteRenderer.color.a == 1)
                {
                    spriteRenderer.color = new Color(255, 255, 255, 0);
                }
                else
                {
                    spriteRenderer.color = new Color(255, 255, 255, 1);
                }
                if (timeSinceBotDeath >= 5)
                {
                    Destroy(gameObject);
                }
            }
            timeSinceBotDeath += Time.deltaTime;
        }
    }

    //novo state depois de morto para deletar corpos de bot
}
