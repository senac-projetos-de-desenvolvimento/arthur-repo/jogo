﻿using UnityEngine;
using System.Collections;

public class BaseFallen : MonoBehaviour {

    protected GameObject HitBox;

    // Use this for initialization
    void OnEnable () {
        (transform.Find("CharacterHitBox").gameObject).SetActive(false);
    }
	
	// Update is called once per frame
	void OnDisable () {
        (transform.Find("CharacterHitBox").gameObject).SetActive(true);
    }
}
