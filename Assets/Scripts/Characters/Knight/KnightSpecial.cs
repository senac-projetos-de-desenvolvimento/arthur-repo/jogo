﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightSpecial : MonoBehaviour{

    protected BaseMain baseMain;

    protected bool up;
    protected bool down;

    public GameObject KnightSpecialHit;
    public AudioClip AttackSound;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
    }

    void OnEnable()
    {
        if (GetComponent<Transform>().rotation.y == 0)
        {
            baseMain.movementV.x = 10;
        }
        else if (GetComponent<Transform>().rotation.y == -1)
        {
            baseMain.movementV.x = -10;
        }
        baseMain.movementV.z = 0;
        baseMain.movementV.y = 0;
        GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);
        KnightSpecialHit.SetActive(true);
        baseMain.animator.SetBool("SpecialUpAtk", false);
        if (AttackSound != null)
        {
            baseMain.audioSource.PlayOneShot(AttackSound, 0.5f);
        }
    }
    void OnDisable()
    {
        KnightSpecialHit.SetActive(false);
    }

    private void Update()
    {
        // Z movement
        if (baseMain.up && !baseMain.down)
        {
            baseMain.movementV.z = baseMain.movement.zspeed * 1;
        }
        else if (baseMain.down && !baseMain.up)
        {
            baseMain.movementV.z = baseMain.movement.zspeed * -1;
        }
        else
        {
            baseMain.movementV.z = 0;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //baseMain.fallSpeed += Physics.gravity.y * Time.deltaTime;
        //baseMain.movementV.y = baseMain.fallSpeed;
        //baseMain.movementV.y = baseMain.movementV.y + (Physics.gravity.y * Time.deltaTime);

        GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);
    }
}
