﻿using UnityEngine;
using System.Collections;

public class MinotaurAttack1 : MonoBehaviour {

    protected BaseMain baseMain;

    public GameObject Hit;
    public AudioClip AttackSound;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
    }

    void OnEnable ()
    {
        baseMain.animator.SetBool("Attack", false);
		Hit.SetActive(true);
        baseMain.specialStatus.armored = true;
        if (AttackSound != null)
        {
            baseMain.audioSource.PlayOneShot(AttackSound);
        }
    }
    void Update()
    {

    }
	void OnDisable() 
    {
		Hit.SetActive(false);
        baseMain.specialStatus.armored = false;
    }
}
