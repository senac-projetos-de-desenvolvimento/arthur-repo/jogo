﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemSpecial : MonoBehaviour{

    protected BaseMain baseMain;

    protected bool up;
    protected bool down;

    public GameObject StartingLocation;
    public GameObject GolemSpecialEffect;
    public AudioClip AttackSound;
    public AudioClip AttackEndSound;

    public float speedMultiplier = 1;
    public float jumpPower = 20;

    protected float raycastRange;
    protected LayerMask isGroundMask;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
        raycastRange = baseMain.raycastRange;
        isGroundMask = baseMain.isGroundMask;
    }

    void OnEnable()
    {

        baseMain.movementV.x = 0;
        baseMain.movementV.y = jumpPower;
        baseMain.movementV.z = 0;
        GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);

        //GolemSpecialHit.SetActive(true);

        if (AttackSound != null)
        {
            baseMain.audioSource.clip = AttackSound;
            baseMain.audioSource.time = 0.8f;
            baseMain.audioSource.Play();
            //baseMain.audioSource.PlayOneShot(AttackSound, 0.5f);
        }
    }
    void OnDisable()
    {
        //GolemSpecialHit.SetActive(false);
    }

    private void Update()
    {
        // Z movement
        if (baseMain.up && !baseMain.down)
        {
            baseMain.movementV.z = baseMain.movement.zspeed * speedMultiplier;
        }
        else if (baseMain.down && !baseMain.up)
        {
            baseMain.movementV.z = baseMain.movement.zspeed * -speedMultiplier;
        }
        else
        {
            baseMain.movementV.z = 0;
        }
        // X movement
        if (baseMain.right && !baseMain.left)
        {
            baseMain.movementV.x = baseMain.movement.xspeed * speedMultiplier;
        }
        else if (baseMain.left && !baseMain.right)
        {
            baseMain.movementV.x = baseMain.movement.xspeed * -speedMultiplier;
        }
        else
        {
            baseMain.movementV.x = 0;
        }

        if(IsGrounded() && baseMain.movementV.y < 0)
        {
            if (AttackEndSound != null)
            {
                baseMain.audioSource.clip = AttackEndSound;
                baseMain.audioSource.time = 0.5f;
                baseMain.audioSource.Play();
                //baseMain.audioSource.PlayOneShot(AttackEndSound);
            }
            GameObject golemSpecialEffect = (GameObject)Instantiate(GolemSpecialEffect,
                                                          StartingLocation.transform.position,
                                                          GolemSpecialEffect.transform.rotation);
            golemSpecialEffect.layer = LayerMask.NameToLayer("Team" + baseMain.charaT + "Hit");
            /*if (baseMain.animator.gameObject.GetComponent<SpriteRenderer>().flipX)
            {
                golemSpecialEffect.GetComponent<Projectile>().Speed.x = -golemSpecialEffect.GetComponent<Projectile>().Speed.x;
            }*/

            //Hit.SetActive(true);
            
            baseMain.animator.SetBool("SpecialUpAtk", false);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //baseMain.fallSpeed += Physics.gravity.y * Time.deltaTime;
        //baseMain.movementV.y = baseMain.fallSpeed;
        //baseMain.movementV.y = baseMain.movementV.y + (Physics.gravity.y * Time.deltaTime);
        baseMain.movementV.y--;
        GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);
    }

    bool IsGrounded() //verify if character is touching the ground
    {
        return Physics.Raycast(transform.position, -Vector3.up, raycastRange, isGroundMask);
    }
}
