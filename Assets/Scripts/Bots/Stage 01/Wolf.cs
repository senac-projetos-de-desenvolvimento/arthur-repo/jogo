﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wolf : MonoBehaviour
{
    protected BaseMain baseMain;
    protected GameObject[] players;
    protected float? distance;

    protected GameObject playerAlvo;
    protected float lastAttackTime = 0;

    protected float attackCooldown = 5; //Cooldown in seconds

    // Start is called before the first frame update
    void Start()
    {
        baseMain = GetComponent<BaseMain>();
    }

    // Update is called once per frame
    void Update()
    {
        distance = null;
        players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length > 0)
        {
            //playerAlvo = players[0];
            foreach (GameObject player in players)
            {
                if (distance == null || distance > Vector3.Distance(transform.position, player.transform.position))
                {
                    playerAlvo = player;
                    distance = Vector3.Distance(transform.position, player.transform.position);
                }
            }

            // Bot vertical movement
            if (transform.position.z - playerAlvo.transform.position.z < -0.5)
            {
                baseMain.up = true;
                baseMain.down = false;
            }
            else if (transform.position.z - playerAlvo.transform.position.z > 0.5)
            {
                baseMain.up = false;
                baseMain.down = true;
            }
            else
            {
                baseMain.up = false;
                baseMain.down = false;
            }
            // Bot Horizontal movement
            if (transform.position.x - playerAlvo.transform.position.x < -1)
            {
                baseMain.right = true;
                baseMain.left = false;
            }
            else if (transform.position.x - playerAlvo.transform.position.x > 1)
            {
                baseMain.right = false;
                baseMain.left = true;
            }
            else
            {
                baseMain.right = false;
                baseMain.left = false;
            }
            // Bot Attack
            if (Mathf.Abs(transform.position.x - playerAlvo.transform.position.x) < 1 && Mathf.Abs(transform.position.z - playerAlvo.transform.position.z) < 0.5 && lastAttackTime <= Time.time)
            {
                baseMain.attack = true;
                lastAttackTime = Time.time + attackCooldown;
            }
            else
            {
                baseMain.attack = false;
            }
        }
    }
}
