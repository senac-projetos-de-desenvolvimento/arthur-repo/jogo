﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeleton : MonoBehaviour
{
    protected BaseMain baseMain;
    protected GameObject[] players;
    protected float? distance;

    protected GameObject playerAlvo;
    protected float lastAttackTime = 0;
    protected float lastSpecialTime = 0;

    public bool hasCombo = true;
    public float attackCooldown = 5; //Cooldown in seconds
    public float attackRange = 2.4f;

    public bool hasSpecial = false;
    public float specialCooldown = 30; //Cooldown in seconds
    public float specialRange = 5f;

    // Start is called before the first frame update
    void Start()
    {
        baseMain = GetComponent<BaseMain>();
    }

    // Update is called once per frame
    void Update()
    {
        distance = null;
        players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length > 0)
        {
            //playerAlvo = players[0];
            foreach (GameObject player in players)
            {
                if (distance == null || distance > Vector3.Distance(transform.position, player.transform.position))
                {
                    playerAlvo = player;
                    distance = Vector3.Distance(transform.position, player.transform.position);
                }
            }

            // Bot vertical movement
            if (transform.position.z - playerAlvo.transform.position.z < -0.5)
            {
                baseMain.up = true;
                baseMain.down = false;
            }
            else if (transform.position.z - playerAlvo.transform.position.z > 0.5)
            {
                baseMain.up = false;
                baseMain.down = true;
            }
            else
            {
                baseMain.up = false;
                baseMain.down = false;
            }
            // Bot Horizontal movement
            if (transform.position.x - playerAlvo.transform.position.x < -attackRange)
            {
                baseMain.right = true;
                baseMain.left = false;
            }
            else if (transform.position.x - playerAlvo.transform.position.x > attackRange)
            {
                baseMain.right = false;
                baseMain.left = true;
            }
            else
            {
                if(baseMain.rotatedRight && playerAlvo.transform.position.x < transform.position.x)
                {
                    baseMain.right = false;
                    baseMain.left = true;
                }
                else if(baseMain.rotatedLeft && playerAlvo.transform.position.x > transform.position.x)
                {
                    baseMain.right = true;
                    baseMain.left = false;
                }
                else
                {
                    baseMain.right = false;
                    baseMain.left = false;
                }
            }
            // Bot Special
            if (hasSpecial)
            {
                if ((Mathf.Abs(transform.position.x - playerAlvo.transform.position.x) < specialRange &&
                Mathf.Abs(transform.position.z - playerAlvo.transform.position.z) < 0.5 &&
                lastSpecialTime <= Time.time))
                {
                    baseMain.special = true;
                    lastAttackTime = Time.time + attackCooldown;
                    lastSpecialTime = Time.time + specialCooldown;
                }
                else
                {
                    baseMain.special = false;
                }
            }
            // Bot Attack
            if ((Mathf.Abs(transform.position.x - playerAlvo.transform.position.x) < attackRange &&
                Mathf.Abs(transform.position.z - playerAlvo.transform.position.z) < 0.5 &&
                lastAttackTime <= Time.time) || 
                (baseMain.hitconfirmed > 0 && hasCombo))
            {
                baseMain.attack = true;
                lastAttackTime = Time.time + attackCooldown;
            }
            else
            {
                baseMain.attack = false;
            }
        }
    }
}
