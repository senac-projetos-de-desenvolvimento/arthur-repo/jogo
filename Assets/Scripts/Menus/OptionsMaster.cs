using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsMaster : MonoBehaviour
{
    public Text volumeText;
    public Text difficultyText;

    int volume = 100;
    int difficulty = 2;
    // Start is called before the first frame update
    void Start()
    {
        volume = PlayerPrefs.GetInt("Volume", 100);
        difficulty = PlayerPrefs.GetInt("Difficulty", 2);

        volumeText.text = volume + "%";
        UpdateDifficulty();
    }

    // Update is called once per frame
    void UpdateDifficulty()
    {
        string difficultyString;
        if (difficulty <= 1)
        {
            difficultyString = "Facil";
        }
        else if (difficulty == 2)
        {
            difficultyString = "Normal";
        }
        else
        {
            difficultyString = "Dificil";
        }
        difficultyText.text = difficultyString;
    }

    public void ChangeDifficulty(bool add)
    {
        if (add)
        {
            
            if(difficulty >= 3)
            {
                difficulty = 1;
            }
            else
            {
                difficulty += 1;
            }
        }
        else
        {
            if(difficulty <= 1)
            {
                difficulty = 3;
            }
            else
            {
                difficulty -= 1;
            }
        }
        UpdateDifficulty();
    }

    public void ChangeVolume(bool add)
    {
        if (add)
        {
            if(volume == 100)
            {

            }
            else
            {
                volume += 10;
            }
        }
        else
        {
            if (volume == 0)
            {

            }
            else
            {
                volume -= 10;
            }
        }
        volumeText.text = volume + "%";

        AudioSource[] audioSources = FindObjectsOfType(typeof(AudioSource), true) as AudioSource[];
        foreach (AudioSource source in audioSources)
        {
            source.volume = volume / 100f;
        }
    }

    public void SaveAndBack(string TelaAlvo)
    {
        PlayerPrefs.SetInt("Volume", volume);
        PlayerPrefs.SetInt("Difficulty", difficulty);
        PlayerPrefs.Save();
        SceneManager.LoadScene(TelaAlvo);
    }

}
