﻿using UnityEngine;
using System.Collections;

public class CharacterSpawner : MonoBehaviour {

    public GameObject selector;
    public int[] selected;

    public GameObject[] characters;
    protected GameObject currentChar;

    protected Vector3 minimumSpawn = new Vector3(2,1,-7.8f);
    protected Vector3 maximumSpawn = new Vector3(18,1,0);
	// Use this for initialization
	void Start () {
        selector = GameObject.FindGameObjectWithTag("Selector");
        if (selector != null)
        {
            selected = selector.GetComponent<Selector>().selected;
            for (int i = 0; i < selected.Length; i++)
            {
                currentChar = (GameObject)Instantiate(characters[selected[i] - 1],
                                                      new Vector3(Random.Range(minimumSpawn.x, maximumSpawn.x), 1, Random.Range(minimumSpawn.z, maximumSpawn.z)),
                                                      characters[selected[i] - 1].transform.rotation);
                currentChar.GetComponent<BaseMain>().playerN = (i + 1).ToString();
                //currentChar.GetComponent<BaseMain>().charaT = i + 1;//Friendly Fire
                currentChar.GetComponent<BaseMain>().charaT = 1;
                currentChar.SetActive(true);
            }
        }
	}
}
