﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterSelectImageChange : MonoBehaviour {

	public Image selector;
	public Sprite random;
	public Sprite Char1;
	public Sprite Char2;
	public CharacterSelectMenu connectionCS;
	
	// Use this for initialization
	void Start () 
	{
		selector = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if (connectionCS.CharIndex == 1)
		{
			selector.sprite = random;
		}
		if (connectionCS.CharIndex == 2)
		{
			selector.sprite = Char1;
		}
		if (connectionCS.CharIndex == 3)
		{
			selector.sprite = Char2;
		}

	}
}