﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterSelectTextChange : MonoBehaviour {

	public Text texto;
	public CharacterSelectMenu connectionCS;
	
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (connectionCS.CharIndex == 1)
		{
			texto.text = "Random";
		}
		if (connectionCS.CharIndex == 2)
		{
			texto.text = "Character1";
		}
		if (connectionCS.CharIndex == 3)
		{
			texto.text = "Character2";
		}
	}
}
