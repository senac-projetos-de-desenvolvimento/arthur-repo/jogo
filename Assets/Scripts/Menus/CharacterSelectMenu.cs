﻿using UnityEngine;
using System.Collections;

public class CharacterSelectMenu : MonoBehaviour {

	public int CharIndex = 0;
	public ChangeScene FimSelect;
	public Selector Selecionado;


	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () {
		var P1right = Input.GetButtonDown("P1Right");
		var P1left = Input.GetButtonDown("P1Left");
		var P1attack = Input.GetButtonDown("P1Attack");

		if (P1left) 
		{
			CharIndex = CharIndex - 1;
		}
		if (P1right) 
		{
			CharIndex = CharIndex + 1;
		}
		if (CharIndex == 0) 
		{
			CharIndex = 3;
		}
		if (CharIndex == 4) 
		{
			CharIndex = 1;
		}
		if (P1attack)
		{
			//Selecionado.seletor = CharIndex;
			FimSelect.MudarTela("Map 1");
		}
	}
}