﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterSelector : MonoBehaviour {
    public Selector selector;
    public Sprite sprite;
    public Sprite spriteS;
    public int player;
    public int selectedChar;
    public GameObject char1;
    public GameObject char2;

    protected int maxchar = 2;
    // Use this for initialization
    void Start () {
        InvokeRepeating("Blinking", 1f, 0.35f);
    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetButtonDown("P" + player + "Right"))
        {
            selectedChar = selectedChar + 1;
        }
        if (Input.GetButtonDown("P" + player + "Left"))
        {
            selectedChar = selectedChar - 1;
        }
        if (Input.GetButtonDown("P" + player + "Left") || Input.GetButtonDown("P" + player + "Right"))
        {
            if (selectedChar > maxchar)
            {
                selectedChar = 1;
            }
            else if (selectedChar < 1)
            {
                selectedChar = maxchar;
            }

            gameObject.GetComponent<RectTransform>().localPosition = new Vector3(-150 + 100 * selectedChar, 45, 0);
        }

            if (Input.GetButtonDown("P" + player + "Attack"))
        {
            selector.selected[player - 1] = selectedChar;
            Destroy(gameObject);
        }
    }
    void Blinking()
    {
        if (GetComponent<Image>().overrideSprite == sprite) {
            GetComponent<Image>().overrideSprite = spriteS;
            //Debug.Log("blink1");
        }
        else
        {
            GetComponent<Image>().overrideSprite = sprite;
            //Debug.Log("blink2");
        }
    }
}
