﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageEnder : MonoBehaviour
{
    public string NextScene;

    // Start is called before the first frame update
    void Awake()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider Hit)
    {
        if (Hit.gameObject.tag == "Player")
        {
            SceneManager.LoadScene(NextScene);
        }
    }
}
