﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveStarter : MonoBehaviour
{
    public StageMaster stageMaster;

    public GameObject[] bots;

    public AudioClip music;

    // Start is called before the first frame update
    void Awake()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider Hit)
    {
        if (Hit.gameObject.tag == "Player")
        {
            if (music != null)
            {
               GameObject.Find("Main Camera").GetComponent<AudioSource>().clip = music;
                GameObject.Find("Main Camera").GetComponent<AudioSource>().Play();
            }
            stageMaster.StartWave(this);
            GetComponent<BoxCollider>().enabled = false;
        }
    }
}
