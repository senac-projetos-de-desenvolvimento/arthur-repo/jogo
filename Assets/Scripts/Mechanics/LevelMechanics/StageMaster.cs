﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageMaster : MonoBehaviour
{
    public GameObject barrierLeft;
    public GameObject barrierRight;

    public GameObject levelEnd;

    public GameObject gameoverUI;

    protected GameObject[] bots;
    protected GameObject[] players;

    // Start is called before the first frame update
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bots = GameObject.FindGameObjectsWithTag("Bot");
        //Debug.Log("BarrierLeft: "+barrierLeft.transform.position.x);
        if(bots.Length == 0)
        {
            barrierRight.transform.position = new Vector3(levelEnd.transform.position.x +5, barrierRight.transform.position.y, barrierRight.transform.position.z);
        }

        if(players == null)
        {
            players = GameObject.FindGameObjectsWithTag("Player");
        }
        else
        {
            int deadPlayers = 0;
            foreach (GameObject player in players)
            {
                if (player.GetComponent<BaseMain>().Health.HealthRemaining <= 0)
                {
                    deadPlayers++;
                }
            }
            if (deadPlayers == players.Length)
            {
                StartCoroutine(GameOver());
            }
        }
    }

    public void StartWave(WaveStarter waveStarter)
    {
        
        foreach (GameObject bot in waveStarter.bots)
        {
            bot.SetActive(true);
        }
        Transform waveCenter = waveStarter.gameObject.transform;
        //Debug.Log(waveCenter.position.x);
        barrierLeft.transform.position = new Vector3(waveCenter.transform.position.x - 15, barrierLeft.transform.position.y, barrierLeft.transform.position.z);
        barrierRight.transform.position = new Vector3(waveCenter.transform.position.x + 15, barrierRight.transform.position.y, barrierRight.transform.position.z);
    }
    IEnumerator GameOver()
    {
        gameoverUI.SetActive(true);
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("Game Over");
    }
}
