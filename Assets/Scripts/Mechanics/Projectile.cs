using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Vector3 Speed = Vector3.zero;

    public bool FadesOut = false;
    public float FadeSpeed = 0.015f;
    public SpriteRenderer spriteRenderer;

    // Update is called once per frame
    void FixedUpdate()
    {
        GetComponent<Rigidbody>().MovePosition(transform.position + (Time.deltaTime * Speed));
        if (FadesOut)
        {
            spriteRenderer.color = new Color(1,1,1, spriteRenderer.color.a - FadeSpeed);
        }
        if(spriteRenderer.color.a <= 0)
        {
            Destroy(gameObject);
        }
    }
}
