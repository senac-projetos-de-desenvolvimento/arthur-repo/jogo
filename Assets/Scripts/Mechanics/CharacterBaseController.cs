using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharacterBaseController : MonoBehaviour {

    public float Health = 1000;
    public Image HealthBar;
	public string playerN; //Player number
	private Animator animator; //sprite animator
	private Transform transformC; //character's transform
	public float xspeed = 2;
	public float zspeed = 1;
    public float runspeed = 4;
	public float jumpPower = 6.5f;
	private Vector3 movement;
	private CharacterController character;
	private float lastHorizontal = 0;
	private float lastVertical = 0;
	private float fallSpeed = 0; //jump not teleport
    protected bool running = false;
	protected bool runningL = false;
	protected bool runningR = false;

	//controllers
	private bool attack;
	private bool jump;
	private bool defend;
	private bool right;
	private bool left;
	private bool up;
	private bool down;
    private bool rightRun; //Running (get down)
    private bool leftRun;  //Running (get down)
	
	//timers
	protected float defendTime = 0;
	protected float attackTime = 0;
    protected float runTimeR = 0;
    protected float runTimeL = 0;
    
    //hits
    public GameObject Hit1;

	// Use this for initialization
	void Start () {
		character = GetComponent<CharacterController>();
		animator = GetComponent<Animator>();
		transformC = GetComponent<Transform>();
	}
	// Update is called once per frame
	void Update () {
        //get keys
        attack = Input.GetButtonDown("P"+playerN+"Attack");
		jump = Input.GetButtonDown("P"+playerN+"Jump");
		defend = Input.GetButtonDown("P"+playerN+"Defend");
		right = Input.GetButton("P"+playerN+"Right");
		left = Input.GetButton("P"+playerN+"Left");
		up = Input.GetButton("P"+playerN+"Up");
		down = Input.GetButton("P"+playerN+"Down");
        rightRun = Input.GetButtonDown("P" + playerN + "Right");
        leftRun = Input.GetButtonDown("P" + playerN + "Left");
        //run
        /*if (running && leftRun || running && rightRun || running && !left && !right)
        {
            running = false;
        }*/
		if (rightRun && runTimeR + 0.25 > Time.time && NoAction())
        {
            running = true;
			runningR = true;
        }
		if (rightRun && NoAction())
        {
            runTimeR = Time.time;
            runTimeL = 0;
        }
		if (leftRun && runTimeL + 0.25 > Time.time && NoAction())
        {
            running = true;
			runningL = true;
        }
		if (leftRun && NoAction())
        {
            runTimeL = Time.time;
            runTimeR = 0;
        }
        //healthBar
		HealthBar.fillAmount = Health/1000;
		if (Health < 1)
		{
			Destroy(gameObject);
		}
        /*if (running)
        {
            animator.SetBool("Running", true);
        }
        if (!running)
        {
            animator.SetBool("Running", false);
        }*/
    }

	void FixedUpdate () {
		float tempHorizontal = 0;
		float tempVertical = 0;
		movement = Vector3.zero;
		//basic move
		//keys * speed
        /*if (!running)
        {*/
            if (left && NoAction() && !right)
            {
                tempHorizontal = -1 * xspeed;
            }
            else if (right && NoAction() && !left)
            {
                tempHorizontal = 1 * xspeed;
            }
            if (up && NoAction() && !down)
            {
                tempVertical = 1 * zspeed;
            }
            else if (down && NoAction() && !up)
            {
                tempVertical = -1 * zspeed;
            }
        /*}/*
        /*else
        {
			if (left && NoAction())
			{
				tempHorizontal = -1 * runspeed;
			}
			else if (right && NoAction())
			{
				tempHorizontal = 1 * runspeed;
			}
			if (up && NoAction())
			{
				tempVertical = 1 * zspeed;
			}
			else if (down && NoAction())
			{
				tempVertical = -1 * zspeed;
			}
        }*/

		if(IsGrounded() && !running){					//possible actions when on ground, walking and iddle
			//Debug.Log("grounded");
			fallSpeed = -1;
			animator.SetBool("JumpAir", false);
			if(tempVertical != 0  || tempHorizontal != 0){ //walking
				animator.SetBool("Walking", true);
			}

			if (attack && NoAction ()) //Attack
			{
				animator.SetInteger ("Attack", 1);
				tempHorizontal = 0.00f;
				tempVertical = 0.00f;
				attackTime = Time.time + 0.416f;
                Hit1.SetActive(true);
			}
			if (Time.time > attackTime) 
			{
				animator.SetInteger ("Attack",0);
                Hit1.SetActive(false);
			}

			if (defend && NoAction ()) //Defend
			{
				animator.SetBool("Defend", true);
				tempHorizontal = 0;
				tempVertical = 0;
				defendTime = Time.time + 0.5f;
			}
			if (Time.time > defendTime) 
			{
				animator.SetBool("Defend", false);
			}

			movement = new Vector3(tempHorizontal, 0, tempVertical);
			//jump activate
			if(jump && NoAction ()){
				lastHorizontal = tempHorizontal * 0.7f;
				lastVertical = tempVertical * 0.7f;
				fallSpeed = jumpPower;

			}
		}
		if (IsGrounded () && running) {      //possible actions when running
			if (runningL) {
				movement = new Vector3(-runspeed, 0, tempVertical);
				animator.SetBool("Running", true);
				if (rightRun || right){
					running = false;
					runningL = false;
					animator.SetBool("Running", false);
				}
			}
			else if (runningR){
				movement = new Vector3(runspeed, 0, tempVertical);
				animator.SetBool("Running", true);
				if (leftRun || left){
					running = false;
					runningR = false;
					animator.SetBool("Running", false);
				}
			}
            if (attack)
            {
                running = false;
                runningL = false;
                runningR = false;
                animator.SetBool("Attack", true);
                animator.SetBool("Running", false);
            }
		}

		fallSpeed += Physics.gravity.y * Time.deltaTime;

		if (!IsGrounded()) {                //possible actions when falling
			//Debug.Log("not grounded");	
			animator.SetBool("JumpAir", true);
			movement = new Vector3 (lastHorizontal, movement.y, lastVertical);
		}


		if(tempVertical == 0 && tempHorizontal == 0){
			animator.SetBool("Walking", false);
		}
		//Rotating
		
		if (right && !left)
		{
			transformC.localScale = new Vector3(2,2,0.5f);
		}
		else if (left && !right)
		{
			transformC.localScale = new Vector3(-2,2,0.5f);
		}


		movement.y = fallSpeed;
		character.Move (movement * Time.deltaTime);
	}

	bool IsGrounded () //verify if character is touching the ground
	{
		return Physics.Raycast (transform.position, - Vector3.up, 0.33f);
	}
	bool NoAction ()
	{
		if (Time.time > attackTime && Time.time > defendTime) {
			return true;
		}
		else {
			return false;
		}
	}
}
