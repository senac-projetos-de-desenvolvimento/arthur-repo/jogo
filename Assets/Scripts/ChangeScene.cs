﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

	public AudioSource audioSource;
	public AudioClip UISound;
	
	public void MudarTela (string TelaAlvo) {
		audioSource.PlayOneShot(UISound);
        SceneManager.LoadScene(TelaAlvo);
	}
}
